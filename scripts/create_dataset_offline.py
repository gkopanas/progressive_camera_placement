from blender.blender_dataset import BlenderDataset
import numpy as np
from blender.blender_dataset import BlenderDataset
from tqdm import tqdm
from blender.general_utils import sample_nodes_uniformgrid, compute_KSMetric_PerNode
from blender.camera_utils import sample_N_reasonbleCameras
import torch
import os
import time

#BLENDER_DIR = r"F:\gkopanas\active_sample_selection\scenes\blender\room_blender\room_insideout_2.9.blend"
#OUT_DIR = r"F:\ingp_test2\voxel_nodes"

#BLENDER_DIR = r"F:\gkopanas\active_sample_selection\scenes\evermotion\LivingRooms\AI48_001\AI48_001_scaled.blend"
#OUT_DIR = r"F:\gkopanas\active_sample_selection\results\livingroom_001\hemisphere"

#BLENDER_DIR = r"F:\gkopanas\active_sample_selection\scenes\evermotion\LivingRooms\AI48_002\AI48_002_scaled.blend"
#OUT_DIR = r"F:\gkopanas\active_sample_selection\results\livingroom_002\hemisphere"

#BLENDER_DIR = r"F:\gkopanas\active_sample_selection\scenes\evermotion\Kitchens\AI43_005\AI043_005_scaled.blend"
#OUT_DIR = r"F:\gkopanas\active_sample_selection\results\kitchens_005\hemisphere"

#BLENDER_DIR = r"F:\gkopanas\active_sample_selection\scenes\evermotion\Offices\AI33_009\AI33_009_280_scaled.blend"
#OUT_DIR = r"F:\gkopanas\active_sample_selection\results\offices_009\hemisphere"

POOL_DIR =  r"F:\gkopanas\active_sample_selection\results\realscene_001\activeNeRF\camera_pool\transforms_colmap.json"

pool_dataset = BlenderDataset(None, POOL_DIR, ingp_testbed=None, num_cams=0, split='train', downsample=1.0, metric=True, safezone=False)

xyz = sample_nodes_uniformgrid(pool_dataset, resolution=32).float().cuda()

selected_cameras = []

#compute_KSMetric_PerNode(pool_dataset, pool_dataset.data_samples.cameras , xyz)

times = []
for i in range(200):
    start_t = time.time()
    candidate_cams = pool_dataset.data_samples.cameras
    metrics = torch.tensor([compute_KSMetric_PerNode(None, selected_cameras + [cam], xyz)[0].sum() for cam in candidate_cams])
    max_idx = metrics.argmax()
    selected_cameras.append(candidate_cams[max_idx])
    del pool_dataset.data_samples.cameras[max_idx]
    stop_t = time.time()
    times.append(stop_t - start_t)
    print(f"ITER [{i}]: {np.array(times).mean()}")
    print([os.path.basename(cam.img_path) for cam in selected_cameras])


#ours_dataset = BlenderDataset(None, OUT_DIR, ingp_testbed=None, num_cams=0, split='train', downsample=1.0, metric=True, safezone=False)

a =123
