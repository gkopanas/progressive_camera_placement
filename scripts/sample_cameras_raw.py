import torch
import numpy as np
from blender.blender_dataset import BlenderDataset
from tqdm import tqdm
from blender.general_utils import sample_nodes_uniformgrid, compute_KSMetric_PerNode
from blender.camera_utils import sample_N_reasonbleCameras
from blender.visualisation_ps import visualize_poses, visualize_metric

torch.set_default_dtype(torch.float32)
torch.manual_seed(12346787)
np.random.seed(12346778)


OUT_DIR = r"F:\new_scene\transforms.json"
BLENDER_DIR = r"F:\gkopanas\active_sample_selection\scenes\evermotion\LivingRooms\AI48_001\AI48_001_scaled.blend"

train_dataset = BlenderDataset(BLENDER_DIR, OUT_DIR, None, 0, split='train', safezone=True)

#train_dataset.geometry.safezone_aabb_max = train_dataset.geometry.scene_aabb_max
#train_dataset.geometry.safezone_aabb_min = train_dataset.geometry.scene_aabb_min
train_dataset.geometry.safezone_aabb_min = torch.tensor([-2., -2., -2.])
train_dataset.geometry.safezone_aabb_max = torch.tensor([2., 2., 2.])

xyz = sample_nodes_uniformgrid(train_dataset, resolution=32).float().cuda()
#occupancy_value = train_dataset.geometry.occupancy_grid.sample_alpha(xyz.cuda().float()).cpu()
#visualize_metric(train_dataset, xyz.cpu().numpy(), occupancy_value.cpu().numpy())
#occupancy_mask = (occupancy_value > 0.8)
#nodes = xyz[occupancy_mask == True]
nodes = xyz

selected_cameras = []
full_logs = []
spatial_logs = []
angular_logs = []
metric = "random"
for i in tqdm(range(10000)):
    candidate_cams = sample_N_reasonbleCameras(train_dataset, 40, train_dataset.fov, safezone=True)
    #visualize_poses(train_dataset, torch.zeros((1, 3), device="cuda"), torch.stack([cam.pose_mat for cam in candidate_cams]))
    if metric!="random":
        metrics = torch.tensor(
            [compute_KSMetric_PerNode(train_dataset, selected_cameras + [cam], nodes)[0].sum() for cam in
             candidate_cams])
        selected_cameras.append(candidate_cams[metrics.argmax()])
    else:
        selected_cameras.append(candidate_cams[10])

    full_metric, spatial_metric, angular_metric = compute_KSMetric_PerNode(train_dataset, selected_cameras, nodes)

    full_logs.append(full_metric.mean().item())
    spatial_logs.append(spatial_metric.mean().item())
    angular_logs.append(angular_metric.mean().item())
    print(f"{full_metric.mean().item()=} | {spatial_metric.mean().item()=} | {angular_metric.mean().item()=}")
    #visualize_poses(train_dataset, torch.zeros((1, 3), device="cuda"), torch.stack([cam.pose_mat for cam in selected_cameras]))
print("a")
import json
with open("F:/"+metric+"_logs.json", "w") as f:
    json.dump({"full_logs": full_logs,
               "spatial_logs": spatial_logs,
               "angular_logs": angular_logs}, f, indent=True)