import torch
import math
from histogram_batched._C import histogram_batched
from blender.ray_utils import get_ray_directions, get_rays_batched, sample_rays_uniformly
import numpy as np
from plyfile import PlyData, PlyElement

def aabb_plyfile(file):
    plydata = PlyData.read(file)
    xyz = np.stack((plydata['vertex']['x'], plydata['vertex']['y'], plydata['vertex']['z']), axis=1)
    aabb_min = torch.tensor(xyz.min(axis=0))
    aabb_max = torch.tensor(xyz.max(axis=0))

    return aabb_min, aabb_max


def center_radius_plyfile(file):
    plydata = PlyData.read(file)
    xyz = np.stack((plydata['vertex']['x'], plydata['vertex']['y'], plydata['vertex']['z']), axis=1)

    aabb_min = torch.tensor(xyz.min(axis=0))
    aabb_max = torch.tensor(xyz.max(axis=0))

    center = xyz.mean(axis=0)
    radius = (aabb_max - aabb_min).max()
    return center, radius


def listify_matrix(matrix):
    matrix_list = []
    for row in matrix:
        matrix_list.append(list(row))
    return matrix_list

def listify_tensor(matrix):
    matrix_list = []
    for row in matrix:
        matrix_list.append([i.item() for i in row])
    return matrix_list

def focal2fov(focal, pixels):
    return 2*math.atan(pixels/(2*focal))

def fov2focal(fov, pixels):
    return pixels / (2 * math.tan(fov / 2))

def sample_nodes_uniformgrid(dataset, resolution=16):

    x, y, z = torch.meshgrid(torch.arange(resolution), torch.arange(resolution), torch.arange(resolution))
    x = (x / (resolution-1)) * (dataset.geometry.scene_aabb_max[0] - dataset.geometry.scene_aabb_min[0]) + dataset.geometry.scene_aabb_min[0]
    y = (y / (resolution-1)) * (dataset.geometry.scene_aabb_max[1] - dataset.geometry.scene_aabb_min[1]) + dataset.geometry.scene_aabb_min[1]
    z = (z / (resolution-1)) * (dataset.geometry.scene_aabb_max[2] - dataset.geometry.scene_aabb_min[2]) + dataset.geometry.scene_aabb_min[2]

    xyz = torch.stack((x, y, z), dim=-1).view(-1, 3)

    return xyz

def sample_density_from_ingp_nn(testbed, train_dataset, query_points, res=32):
    density, _ = sample_density_from_ingp(testbed, train_dataset, res=32)
    blurred_density = torch.clamp(blur_density(density), 0.0, 1.0)

    aabb_min = train_dataset.geometry.scene_aabb_min.cuda().unsqueeze(0)
    aabb_max = train_dataset.geometry.scene_aabb_max.cuda().unsqueeze(0)
    query_idx = torch.clamp(torch.round(((query_points - aabb_min)/(aabb_max - aabb_min))*(res-1)).long(), 0, res-1)

    return blurred_density[query_idx[...,0], query_idx[...,1], query_idx[..., 2]]

def sample_density_from_ingp(testbed, train_dataset, res=32):
    nodes = sample_nodes_uniformgrid(train_dataset, res).float().cuda()

    ingp_nodes = torch.zeros_like(nodes)
    ingp_nodes[:,0] = nodes[:, 1]
    ingp_nodes[:,1] = nodes[:, 2]
    ingp_nodes[:,2] = nodes[:, 0]

    ingp_nodes = ingp_nodes*0.330 + 0.5

    ingp_nodes = (ingp_nodes+7.5)/16.0

    density = torch.zeros(ingp_nodes.shape[0], dtype=torch.uint8)
    testbed.gk_test2(ingp_nodes.data_ptr(), density.data_ptr(), ingp_nodes.shape[0])
    density = density.cuda()/255.0

    density = density.view(res, res, res)
    density = density.permute(2,1,0)

    return density, nodes

def blur_density(density_grid):
    kernel = torch.ones((1,1,3,3,3)).cuda()
    out = torch.nn.functional.conv3d(density_grid.unsqueeze(0).unsqueeze(0), kernel,  padding='same')
    out = out.squeeze().permute(2,1,0)
    return out


def get_ingp_grid_samples(resolution):
    x, y, z = torch.meshgrid(torch.arange(resolution), torch.arange(resolution), torch.arange(resolution))

    xyz = torch.stack(((z/(resolution-1)), (y/(resolution-1)),  (x/(resolution-1))), dim=-1).view(-1, 3)
    return xyz

def get_xyz_correspond_to_ingp_samples(resolution):

    x, y, z = torch.meshgrid(torch.arange(resolution), torch.arange(resolution), torch.arange(resolution))

    xyz = torch.stack(((z/(resolution-1))*16-7.5, (y/(resolution-1))*16-7.5,  (x/(resolution-1))*16-7.5), dim=-1).view(-1, 3)

    nodes2 = torch.zeros_like(xyz)

    tmp = (xyz - 0.5) / 0.330
    nodes2[:, 0] = tmp[:, 2]
    nodes2[:, 1] = tmp[:, 0]
    nodes2[:, 2] = tmp[:, 1]

    return nodes2


def getDirsOnPolar(dirs, H, W):
    def cartesian_to_polar(xyz):
        ptsnew = torch.cat((xyz, torch.zeros(xyz.shape, device="cuda")), dim=-1)
        xy = xyz[..., 0] ** 2 + xyz[..., 1] ** 2
        ptsnew[..., 3] = torch.sqrt(xy + xyz[..., 2] ** 2)
        ptsnew[..., 4] = torch.arctan2(torch.sqrt(xy), xyz[..., 2])  # for elevation angle defined from Z-axis down
        # ptsnew[:,4] = np.arctan2(xyz[:,2], np.sqrt(xy)) # for elevation angle defined from XY-plane up
        ptsnew[..., 5] = torch.arctan2(xyz[..., 1], xyz[..., 0])
        return ptsnew

    # r, theta, phi

    sph_coord = cartesian_to_polar(dirs)[..., 3:].float()
    sph_coord[..., 1] = sph_coord[..., 1] / math.pi  # from 0 - pi to 0 - 1
    sph_coord[..., 2] = ((sph_coord[..., 2] + math.pi) / (2 * math.pi))  # from -pi - pi to 0 - 1

    # 0 - 1 to 0 - H/W
    y = sph_coord[..., 1] * H
    x = sph_coord[..., 2] * W

    return x,y

def project_points_to_many_cams(cameras, points):
    hom_points = torch.cat((points, torch.tensor([[1.0]], device="cuda").repeat(points.shape[0], 1)), dim=1)
    pose_mats = torch.stack([torch.inverse(cam.pose_mat) for cam in cameras])
    wierd_mats = torch.stack([cam.wierd_mat for cam in cameras])


    camera_space_points = torch.einsum('lkj, ij -> ilk', pose_mats, hom_points.cuda().float())
    camera_space_points[:,:,0:2] /= camera_space_points[:,:,2:3]
    camera_space_points = camera_space_points[:,:,:3]
    wierd_points = torch.einsum('fkj, ifk -> ifk', wierd_mats, camera_space_points)

    return wierd_points

def point_camera_mask(cameras, points):

    proj_mats = torch.stack([cam.get_proj_mat() for cam in cameras], dim=0)

    N_POINTS = points.shape[0]

    # Homogenify
    #hom_points = torch.cat((points, torch.tensor([[1.0]]).repeat(N_POINTS, 1)), dim=1)
    # Project Point to all cameras
    #projected_points = torch.einsum('lkj, ij -> ilk', proj_mats, hom_points.cuda().float())

    projected_points = project_points_to_many_cams(cameras, points)

    positive_z_filter = projected_points[:, :, 2:3] > 0
    #projected_points_divz = projected_points/projected_points[:, : , 2:3]

    a = torch.tensor([c.w for c in cameras])
    assert (a - a[0]).sum()==0, f"{a=}"
    incamera_filter = (projected_points[:, :, 0:1] > -cameras[0].w/2.0) & (projected_points[:, :, 0:1] < cameras[0].w/2.0) & \
                      (projected_points[:, :, 1:2] > -cameras[0].h/2.0) & (projected_points[:, :, 1:2] < cameras[0].h/2.0)

    return torch.logical_and(positive_z_filter, incamera_filter)

def point_camera_mask2(cameras):
    poses = torch.stack([cam.pose_mat for cam in cameras])

    directions = get_ray_directions(800, 800, [focal_x, focal_y], center=[w / 2.0, h / 2.0]).cuda()
    rays = get_rays_batched(directions, poses)
    #visualize_poses_and_rays(dataset,rays)
    ray_samples = sample_rays_uniformly(rays)

def per_pixel_area_of_sphere(H, W):
    D_theta = math.pi / H
    D_phi = 2 * math.pi / W
    perpixel_area = torch.sin(((torch.arange(H) + 0.5) / H) * math.pi)[:, None].repeat(1, W) * D_theta * D_phi

    return perpixel_area

def compute_KSMetric_PerNode(dataset, cameras, nodes):
    origins = torch.stack([cam.world_o for cam in cameras], dim=0)
    poses = torch.stack([cam.pose_mat for cam in cameras], dim=0)

    point_cam_mask = point_camera_mask(cameras, nodes)
    #visualize_metric(dataset, nodes.cpu().numpy(), point_cam_mask[:,0:1,...].squeeze().cpu().numpy())

    N_CAMERAS = origins.shape[0]
    N_NODES = nodes.shape[0]
    W = 150
    H = 150

    origins = origins.unsqueeze(0).repeat(N_NODES, 1, 1)
    nodes = torch.tensor(nodes).unsqueeze(1).repeat(1, N_CAMERAS, 1)
    NC_vector = nodes - origins

    # Get camera directiosn per node
    x, y = getDirsOnPolar(NC_vector, H, W)

    cdf_resolution = 5

    pdf_u = torch.ones((cdf_resolution, 2 * cdf_resolution), device="cuda") / (cdf_resolution * 2 * cdf_resolution)
    uniform_angular_cdf = torch.cumsum(torch.cumsum(pdf_u, axis=0), axis=1)

    hist_gk = histogram_batched(x.cuda(),
                                y.cuda(),
                                point_cam_mask.int(),
                                cdf_resolution,
                                0, W, 0, H)
    hist_gk_area_weighted = hist_gk/per_pixel_area_of_sphere(cdf_resolution, cdf_resolution*2).unsqueeze(0).cuda()
    pdf_gk = hist_gk_area_weighted/hist_gk_area_weighted.sum(dim=1, keepdim=True).sum(dim=2, keepdim=True).clamp_min(0.00001)

    #pdf_gk = hist_gk/hist_gk.sum(dim=1, keepdim=True).sum(dim=2, keepdim=True).clamp_min(0.00001)

    #angular_distance_pdf = torch.abs(pdf_gk - pdf_u[None, ...]).mean(dim=-1).mean(dim=-1)


    cdf_gk = torch.cumsum(torch.cumsum(pdf_gk, axis=1), axis=2)
    cdf_norm_gk = cdf_gk
    #cdf_norm_gk = cdf_gk/torch.clamp_min(point_cam_mask.sum(dim=1)[...,None], 0.00001)
    #angular_distance = 1 - torch.abs(cdf_norm_gk - uniform_angular_cdf[None,...]).mean(dim=-1).mean(dim=-1)
    angular_distance = 1 - torch.abs(pdf_gk - pdf_u).sum(dim=-1).sum(dim=-1)/2.0
    angular_distance[(pdf_gk.view(pdf_gk.shape[0], -1)==0.0).all(dim=1)] = 0.0

    #uniform_pdf = torch.ones(point_cam_mask.shape[0], device="cuda")/point_cam_mask.shape[0]
    empirical_pdf = point_cam_mask.squeeze(-1).float().sum(dim=1)/point_cam_mask.float().sum()
    spatial_distance = point_cam_mask.squeeze(-1).float().sum(dim=1)/len(cameras)
    #uniform_spatial_cdf = torch.cumsum(uniform_pdf, axis=0)
    #empirical_spatial_cdf = torch.cumsum(empirical_pdf, axis=0)
    #metric_gk += 1 - torch.abs(uniform_spatial_cdf - empirical_spatial_cdf)
    #spatial_distance = (1 - torch.abs(empirical_pdf - uniform_pdf)/torch.max(torch.stack((empirical_pdf, uniform_pdf)), dim=0).values)

    # Observe as much as possible
    observe_as_much = point_cam_mask.float().mean(dim=1).squeeze()*0.1

    #point_cam_mask.float().sum(dim=1)/point_cam_mask.float().sum()
    #metric_gk +=
    metric_gk = angular_distance + torch.pow(spatial_distance, 0.1/2.0)

    torch.cuda.empty_cache()

    return metric_gk, spatial_distance, angular_distance