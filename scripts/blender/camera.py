import torch
from blender.ray_utils import get_ray_directions, get_rays

class Camera():
    def __init__(self, pose, focal_x, focal_y, h, w, img_path=None):

        self.img_path = img_path

        self.focal_x = focal_x
        self.focal_y = focal_y
        self.h = h
        self.w = w

        self.pose_mat = pose.cuda()
        self.world_o = pose[:3, 3].cuda()

        directions = get_ray_directions(h, w, [focal_x, focal_y], center=[w/2.0, h/2.0]).to(pose.device)  # (h, w, 3)
        self.rays = get_rays(directions, pose).cuda()  # (h*w, 6)

        self.intr_mat = torch.tensor([[self.focal_x, 0 , self.w/2.0],
                                      [0, self.focal_y, self.h/2.0],
                                      [0, 0, 1]]).float().cuda()
        self.wierd_mat = torch.tensor([[self.focal_x, 0., 0.],
                                      [0, self.focal_y, 0.],
                                      [0, 0, 1]]).float().cuda()

    def get_proj_mat(self):
        return self.intr_mat @ self.pose_mat[:3,:]

    def project_points(self, points):
        tmp = self.pose_mat[:3, :] @ points
        tmp /= tmp[:, 2]
        final = self.intr_mat@tmp

        return final
