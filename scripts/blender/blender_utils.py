import os
try:
    import bpy
    import mathutils
except:
    pass
import numpy as np
import json
import pymeshlab
import math
import argparse
import configargparse
import sys
from blender.general_utils import listify_matrix

class BlenderScene():
    def __init__(self, blender_scenepath, output_folder, fov, aabb_scale=16):
        print(f"Opening Blender Scene {blender_scenepath}...", end="")
        bpy.ops.wm.open_mainfile(filepath=blender_scenepath)
        print("Done")

        self.fov = fov
        # USE GPU TO RENDER
        bpy.context.scene.cycles.device = 'GPU'
        bpy.context.preferences.addons['cycles'].preferences.compute_device_type = 'OPTIX'

        self.generated_data_path = bpy.path.abspath("//{}".format(output_folder))
        if not os.path.exists(self.generated_data_path):
            os.makedirs(self.generated_data_path)
        print("All output files will be in {}".format(self.generated_data_path))

        # Create main cam object and set Fov
        self.cam = bpy.data.cameras.new("test_script_cam")
        self.cam.angle = fov
        # Create object
        self.cam = bpy.data.objects.new("test_script_cam", self.cam)


    def set_cam_posdir(self, pos, dir):
        self.cam.location = pos
        rot_quat = dir.to_track_quat('-Z', 'Y')
        self.cam.rotation_euler = rot_quat.to_euler()
    """
    def render_camera_byname(self, camera_name, subfolder, prefix="cam"):
        bpy.data.scenes["Scene"].camera = bpy.context.scene.objects[camera_name]
        rel_path = os.path.join(subfolder, "{}_{}.png".format(prefix, len(self.scene_dictionary["frames"])))
        bpy.context.scene.render.filepath = os.path.join(self.generated_data_path, rel_path)
        bpy.ops.render.render(write_still=True)
        bpy.context.view_layer.update()
        frame_data = {'file_path': rel_path,
                      'rotation': 0.0,
                      'transform_matrix': listify_matrix(bpy.context.scene.objects[camera_name].matrix_world)}

        # Store info
        self.scene_dictionary['frames'].append(frame_data)

        return frame_data
    """
    def render_camera(self, pos, dir, rel_path):
        self.set_cam_posdir(pos, mathutils.Vector(dir))
        bpy.data.scenes["Scene"].camera = self.cam
        bpy.data.scenes["Scene"].render.resolution_x = 800
        bpy.data.scenes["Scene"].render.resolution_y = 800
        bpy.context.scene.render.filepath = os.path.join(self.generated_data_path, rel_path)
        bpy.ops.render.render(write_still=True)
        bpy.context.view_layer.update()
        print(f"{self.cam.matrix_world=}")
        frame_data = {'file_path': rel_path, 'rotation': 0.0, 'transform_matrix': listify_matrix(self.cam.matrix_world)}#,
                      #"dir": [dir[0] / np.linalg.norm(dir),
                      #        dir[1] / np.linalg.norm(dir),
                      #        dir[2] / np.linalg.norm(dir)],
                      #"pos": [pos[0], pos[1], pos[2]]}

        # Store info

        return frame_data
    """
    def render_depth(self, pos, dir):
        scene = bpy.context.scene
        scene.view_layers["All"].use_pass_z = True

        tree = scene.node_tree

        # Render Layer
        rl_objects_node = tree.nodes["Render Layers"]

        # Create links between render layer depth and the final output
        tree.links.new(rl_objects_node.outputs['Depth'], tree.nodes["Viewer"].inputs['Image'])

        # render
        bpy.ops.render.render()
        # get viewer pixels
        pixels = bpy.data.images['Viewer Node'].pixels
        print(len(pixels))  # size is always width * height * 4 (rgba)
        arr = np.array(pixels[:])
        print(arr)
        print(arr.max())

    """
    def export_geometry(self, file_path):
        name = "geometry"
        obj_path = filepath=os.path.join(file_path, name + ".obj")
        ply_path = filepath=os.path.join(file_path, name + ".ply")

        bpy.ops.export_scene.obj(filepath=obj_path, use_materials=False,
                                 axis_forward='Y', axis_up='Z')

        ms = pymeshlab.MeshSet()
        ms.load_new_mesh(obj_path)
        ms.save_current_mesh(ply_path)

    def get_random_hemisphere_loc(self, radius):
        u = np.random.uniform(0, 1, size=2)
        z = u[0]
        r = math.sqrt(max(0, 1. - z * z))
        phi = 2 * math.pi * u[1]

        return (radius * r * math.cos(phi), radius * r * math.sin(phi), radius * z)

    def render_set_overhemisphere(self, radius, number_of_cameras, inside_out, subfolder="train"):
        for i in range(0, number_of_cameras):
            # Sample location
            pos = self.get_random_hemisphere_loc(radius)
            # Sample direction (Always looking either in the center or the opposite)
            d = -1.0 if inside_out else 1.0
            direction = d * (mathutils.Vector((0.0, 0.0, 0.0)) - mathutils.Vector(pos))
            # Render
            frame_data = self.render_camera(pos, direction, subfolder)

        self.export_json(subfolder)

    def render_set_overpath(self, number_of_cameras, blender_camera_name, subfolder="train"):
        bpy.context.scene.objects[blender_camera_name].data.lens = self.fov
        print(self.fov)
        for i in range(0, number_of_cameras):
            bpy.context.scene.objects[blender_camera_name].constraints.values()[0].offset_factor = i / (number_of_cameras - 1.0)
            frame_data = self.render_camera_byname(blender_camera_name, subfolder)

        self.export_json(subfolder)

    def get_json_path(self, subfolder):
        return os.path.join(self.generated_data_path, 'transforms_{}.json'.format(subfolder))

    def export_json(self, subfolder):
        with open(self.get_json_path(subfolder), 'w') as out_file:
            json.dump(self.scene_dictionary, out_file, indent=4)
