import time
import torch
from blender.ray_utils import get_ray_directions, get_rays_batched, sample_rays_uniformly
from blender.general_utils import fov2focal, sample_density_from_ingp_nn, sample_nodes_uniformgrid, sample_density_from_ingp
from blender.camera import Camera
import os

def get_eye_lookat_from_pose(pose):
    front = pose[...,:3, 2]
    eye = pose[...,:3, 3]
    up = pose[...,:3, 1]
    #look_at = eye - front
    #look_at /= look_at.norm(dim=-1, keepdim=True)

    look_at = torch.einsum("jk, j -> k", pose[:3, :3].transpose(dim0=0, dim1=1),
                            torch.tensor([0.0, 0.0, 1.0], device="cuda"))

    return eye, look_at, up

def get_pose_matrices(look_at, eye, up):
    """
    Returns pose matrices (c2w) from look_at, eye, up.
    The inputs and outputs are batched
    """
    cams, _ = look_at.shape
    front = (eye - look_at)
    front /= front.norm(dim=1, keepdim=True)
    right = torch.cross(up, front)
    right /= right.norm(dim=1, keepdim=True)
    up = torch.cross(front, right)
    up /= up.norm(dim=1, keepdim=True)

    poses = torch.eye(4, device="cuda")[None,...].repeat((cams, 1, 1))
    poses[:, :3, 0] = right
    poses[:, :3, 1] = up
    poses[:, :3, 2] = front
    poses[:, :3, 3] = eye

    return poses

def sample_poses(n, aabb_min_bound, aabb_max_bound):
    """
    Sample N random poses inside aabb
    """

    def sample_spherical(npoints, ndim=3):
        vec = torch.randn([npoints, ndim], device="cuda")
        vec /= vec.norm(dim=1, keepdim=True)
        return vec

    aabb_min = aabb_min_bound
    aabb_max = aabb_max_bound

    x_rand = torch.rand(size=[n], device="cuda")[...,None] * (aabb_max[0] - aabb_min[0]) + aabb_min[0]
    y_rand = torch.rand(size=[n], device="cuda")[...,None] * (aabb_max[1] - aabb_min[1]) + aabb_min[1]
    z_rand = torch.rand(size=[n], device="cuda")[...,None] * (aabb_max[2] - aabb_min[2]) + aabb_min[2]

    positions = torch.cat((x_rand, y_rand, z_rand), dim=1)  # camera position

    lookAts = positions + sample_spherical(n)  # look_at target
    #centers = sample_ronan(n)
    ups = torch.tensor([0., 0., 1.], device="cuda")[None, ...].repeat(n, 1)

    #visualize_sampled_cameras(train_dataset, lookAts, positions)

    poses = get_pose_matrices(lookAts, positions, ups)

    return poses

DEBUG = False

def sample_N_reasonbleCameras(dataset, n, fov, safezone=False):
    """
    Samples cameras by rejecting any camera that observes occupied space from too close.
    """
    t0 = time.time()

    if safezone:
        filtered_poses = sample_poses(n//3, dataset.geometry.safezone_aabb_min, dataset.geometry.safezone_aabb_max)
    else:
        max_aabb_range = torch.abs(dataset.geometry.scene_aabb_min - dataset.geometry.scene_aabb_max).max()

        poses = sample_poses(n, dataset.geometry.scene_aabb_min, dataset.geometry.scene_aabb_max)
        if DEBUG:
            dir = os.path.join(os.path.dirname(dataset.full_json), "DEBUG", str(dataset.data_samples.num))
            os.makedirs(dir, exist_ok=True)
            torch.save(poses, os.path.join(dir, "poses.ts" ))
        h,w = 50, 50
        focal_x = fov2focal(fov, w)
        focal_y = fov2focal(fov, h)


        directions = get_ray_directions(h, w, [focal_x, focal_y], center=[w / 2.0, h / 2.0]).cuda()
        rays = get_rays_batched(directions, poses)
        #visualize_poses_and_rays(dataset,rays)
        ray_samples = sample_rays_uniformly(rays, max_aabb_range)

        #density = dataset.geometry.occupancy_grid.sample_alpha(ray_samples).view(ray_samples.shape[:-1])
        #occupancy_mask = density<0.8

        density = sample_density_from_ingp_nn(dataset.geometry.ingp_testbed, dataset, ray_samples.view([-1, 3])).view(ray_samples.shape[:-1])
        occupancy_mask = density < 0.3
        if DEBUG:
            density, xyz = sample_density_from_ingp(dataset.geometry.ingp_testbed, dataset)

            dir = os.path.join(os.path.dirname(dataset.full_json), "DEBUG", str(dataset.data_samples.num))
            os.makedirs(dir, exist_ok=True)
            torch.save(xyz, os.path.join(dir, "grid_xyz.ts"))
            torch.save(density, os.path.join(dir, "grid_density.ts"))

        # Compute the Depth of all samples.
        depth_of_samples = (ray_samples - rays[:, :, None, :3]).norm(dim=-1)
        # Any sample that is free, gets assigned a artificially big value.
        depth_of_samples[occupancy_mask] = 999999.0
        # So that when we get the min-value for each camera, we get the min depth of the first occupied voxel.
        min_depth = depth_of_samples.view(n, -1).min(dim=1).values

        #visualize_sampled_rays(dataset, ray_samples, occupancy_value, rays[:,0,:3], rays[:,1250,3:], min_depth)

        print(poses.shape)
        filtered_poses = poses[min_depth > 0.03*max_aabb_range]
        if DEBUG:
            dir = os.path.join(os.path.dirname(dataset.full_json), "DEBUG", str(dataset.data_samples.num))
            os.makedirs(dir, exist_ok=True)
            torch.save(poses, os.path.join(dir, "filtered_poses.ts" ))
            torch.save(ray_samples, os.path.join(dir, "ray_samples.ts" ))
            torch.save(density, os.path.join(dir, "ray_samples_density.ts" ))

        print(filtered_poses.shape)
        #visualize_poses(dataset, filtered_poses)

    cameras = [Camera(pose, fov2focal(fov, 800), fov2focal(fov, 800), 800, 800) for pose in filtered_poses]

    print(f"Time {time.time()-t0}")
    torch.cuda.empty_cache()

    return cameras