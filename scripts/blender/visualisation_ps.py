import polyscope as ps
import numpy as np
import torch
import os

def visualize_geometries(dataset, dbg_iter=None):
    ps.init()
    vertices = np.asarray(dataset.geometry.o3dmesh.vertices)
    faces = np.asarray(dataset.geometry.o3dmesh.triangles)
    ps.register_surface_mesh("Scene Geometry", vertices, faces, transparency=0.1)

    vertices = np.asarray( dataset.geometry.scene_bbox.vertices)
    faces = np.asarray( dataset.geometry.scene_bbox.triangles)
    ps.register_surface_mesh("Scene Box", vertices, faces, transparency=0.5)

    vertices = np.asarray( dataset.geometry.scene_safezone.vertices)
    faces = np.asarray( dataset.geometry.scene_safezone.triangles)
    ps.register_surface_mesh("Safe Zone", vertices, faces, transparency=0.7)



    ps.register_point_cloud("roi", dataset.geometry.roi_center[None, ...], enabled=True)

    """
    if dbg_iter:
        dir = os.path.join(os.path.dirname(dataset.full_json), "DEBUG", str(dbg_iter))
        poses = torch.load(os.path.join(dir, "poses.ts"))[:2,...]

        ray_samples = torch.load(os.path.join(dir, "ray_samples.ts"))
        ray_samples_density = torch.load(os.path.join(dir, "ray_samples_density.ts"))

        grid_density = torch.load(os.path.join(dir, "grid_density.ts"))
        grid_xyz = torch.load(os.path.join(dir, "grid_xyz.ts"))

        ps_cameras = ps.register_point_cloud("ray_samples", ray_samples[2:4, ...].view(-1, 3).detach().cpu().numpy(),
                                             enabled=True)
        ps_cameras.add_scalar_quantity("color",
                                      ray_samples_density[2:4, ...].view(-1).detach().cpu().numpy(),
                                      enabled=True)

        ps_cameras = ps.register_point_cloud("grid_density", grid_xyz.view(-1, 3).detach().cpu().numpy(),
                                             enabled=True)
        ps_cameras.add_scalar_quantity("color",
                                      grid_density.flatten().detach().cpu().numpy(),
                                      enabled=True)
    """
    poses = torch.stack([cam.pose_mat for cam in dataset.data_samples.cameras])
    cam_origins = poses[:, :3, 3].cpu().detach().numpy()
    ps_cameras = ps.register_point_cloud("cameras", cam_origins, enabled=True)
    dirs = torch.einsum("ijk, j -> ik", poses[:, :3, :3].transpose(dim0=1, dim1=2),
                        torch.tensor([0.0, 0.0, 1.0], device="cuda"))
    ps_cameras.add_vector_quantity("cam_dirs", dirs.squeeze().detach().cpu().numpy(), enabled=True)

    ps.show()

def visualize_metric(dataset, nodes, metric):
    ps.init()
    ps_vol = ps.register_point_cloud("nodes", nodes, enabled=True)
    ps_vol.add_scalar_quantity("metric", metric, enabled=True)

    try:
        o3dmesh = dataset.geometry.o3dmesh
        vertices = np.asarray(o3dmesh.vertices)
        faces = np.asarray(o3dmesh.triangles)
        ps_mesh = ps.register_surface_mesh("test mesh", vertices, faces, transparency=0.1)
    except:
        print("No Mesh!")

    cam_origins = torch.stack([cam.world_o for cam in dataset.data_samples.cameras], dim=0)
    ps_cameras = ps.register_point_cloud("cameras", cam_origins.detach().cpu().numpy(), enabled=True)

    poses = torch.stack([cam.pose_mat for cam in dataset.data_samples.cameras], dim=0)
    dirs = torch.einsum("ijk, j -> ik", poses[:, :3, :3].transpose(dim0=1, dim1=2),
                        torch.tensor([0.0, 0.0, 1.0], device="cuda"))
    ps_cameras.add_vector_quantity("cam_dirs", dirs.squeeze().detach().cpu().numpy(), enabled=True)

    ps.show()

def visualize_poses(dataset, nodes, poses):
    ps.init()

    if hasattr(dataset.geometry, "o3dmesh"):
        o3dmesh = dataset.geometry.o3dmesh
        vertices = np.asarray(o3dmesh.vertices)
        faces = np.asarray(o3dmesh.triangles)
        ps_mesh = ps.register_surface_mesh("test mesh", vertices, faces, transparency=0.4)

    cam_poses = poses[:,:3, 3].cpu().detach().numpy()
    ps_cameras = ps.register_point_cloud("cameras", cam_poses, enabled=True)

    if nodes is not None:
        ps_nodes = ps.register_point_cloud("nodes", nodes.cpu().detach().numpy(), enabled=True)

    dirs = torch.einsum("ijk, j -> ik", poses[:, :3, :3].transpose(dim0=1, dim1=2),
                        torch.tensor([0.0, 0.0, 1.0], device="cuda"))
    ps_cameras.add_vector_quantity("cam_dirs", dirs.squeeze().detach().cpu().numpy(), enabled=True)

    ps.show()

def visualize_sampled_rays(dataset, ray_samples, occupancy_value, cam_o, cam_d, min_depth):
    ps.init()
    o3dmesh = dataset.geometry.o3dmesh
    vertices = np.asarray(o3dmesh.vertices)
    faces = np.asarray(o3dmesh.triangles)
    ps_mesh = ps.register_surface_mesh("test mesh", vertices, faces)

    ps_cameras = ps.register_point_cloud("cameras", ray_samples[:2,...].view(-1, 3).detach().cpu().numpy(), enabled=True)
    ps_cameras.add_color_quantity("color",
                                  occupancy_value[:2, ...].view(-1)[..., None].repeat(1, 3).detach().cpu().numpy(),
                                  enabled=True)

    ps_cameras1 = ps.register_point_cloud("pc2", cam_o[:2,...].detach().cpu().numpy(), enabled=True)
    ps_cameras2 = ps.register_point_cloud("pc3", (cam_o+cam_d*min_depth[...,None])[:2,...].detach().cpu().numpy(), enabled=True)

    ps.show()


def visualize_poses_and_rays(dataset, rays):
    ps.init()

    o3dmesh = dataset.geometry.o3dmesh
    vertices = np.asarray(o3dmesh.vertices)
    faces = np.asarray(o3dmesh.triangles)
    ps_mesh = ps.register_surface_mesh("test mesh", vertices, faces)

    ps_cameras = ps.register_point_cloud("cameras", rays[:, :, :3].view(-1, 3).detach().cpu().numpy(), enabled=True)
    ps_cameras.add_vector_quantity("cam_dirs", rays[:, :, 3:].view(-1, 3).detach().cpu().numpy(), enabled=True)
    ps.show()


def visualize_sampled_cameras(dataset, lookAts, positions):
    ps.init()
    o3dmesh = dataset.geometry.o3dmesh
    vertices = np.asarray(o3dmesh.vertices)
    faces = np.asarray(o3dmesh.triangles)
    ps_mesh = ps.register_surface_mesh("test mesh", vertices, faces)

    ps_cameras = ps.register_point_cloud("camera_positions", positions.detach().cpu().numpy(), enabled=True)

    dirs = lookAts - positions
    ps_cameras.add_vector_quantity("cam_dirs", dirs.detach().cpu().numpy(), enabled=True)

    ps.show()
