import math
import os
import pickle
import json
import torch
from tqdm import tqdm
#import open3d as o3d
import numpy as np
from torchvision import transforms as T
from PIL import Image
from blender.camera import Camera
from blender.blender_utils import BlenderScene
from blender.general_utils import sample_nodes_uniformgrid, compute_KSMetric_PerNode, sample_density_from_ingp_nn
from blender.camera_utils import sample_N_reasonbleCameras, get_eye_lookat_from_pose
from blender.general_utils import aabb_plyfile, center_radius_plyfile, listify_tensor, sample_density_from_ingp

blender2opencv = np.array([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])
converter = np.array([[-1, 0, 0, 0], [0, 1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])

class Geometry():
    def __init__(self, geometry_folder, ingp_testbed):
        #self.mesh_path = (os.path.join(r"F:/gkopanas/active_sample_selection/scenes/nerf_synthetic/room_lego", "geometry.ply"))
        #self.o3dmesh = o3d.io.read_triangle_mesh(os.path.join(geometry_folder, "geometry.ply"))
        #self.scene_bbox = o3d.io.read_triangle_mesh(os.path.join(geometry_folder, "scene_aabb.ply"))
        #self.scene_safezone = o3d.io.read_triangle_mesh(os.path.join(geometry_folder, "scene_safezone.ply"))

        self.scene_aabb_min, self.scene_aabb_max = aabb_plyfile(os.path.join(geometry_folder, "scene_aabb.ply"))
        self.safezone_aabb_min, self.safezone_aabb_max = aabb_plyfile(os.path.join(geometry_folder, "scene_safezone.ply"))
        self.roi_center, self.roi_radius = center_radius_plyfile(os.path.join(geometry_folder, "scene_centerobject.ply"))

        #self.aabb_min_bound = torch.tensor([-9.86517, -11.1152, -3.58565])
        #self.aabb_max_bound = torch.tensor([9.75824, 8.78434, 10.1975])

        self.ingp_testbed = ingp_testbed
        #with open(r"F:\gkopanas\active_sample_selection\blender_scene_generation\log\fixed\freaking_test_hemisphere\fixed\alphaMask.pkl", 'rb') as alphaMaskFile:
        #    self.occupancy_grid = pickle.load(alphaMaskFile)


class DataSamples():
    def __init__(self, downsample):
        self.cameras = []
        self.all_rgbs = []
        self.downsample = downsample
        self.num = 0

    def get_imgdata_fromfile(self, img_path):
        # Handle ground-truth image:
        # 1) downsample 2) reshape 3) blend A to RGB as if bg is white
        img = Image.open(img_path)
        if self.downsample != 1.0:
            img = img.resize(self.img_wh, Image.LANCZOS)
        img = T.ToTensor()(img)  # (4, h, w)
        ch, h, w = img.shape
        img = img.view(-1, w * h).permute(1, 0)  # (h*w, 4) RGBA
        if img.shape[-1] == 4:
            img = img[:, :3] * img[:, -1:] + (1 - img[:, -1:])  # blend A to RGB

        return img, h, w

    def add_image(self, transform_matrix, focal_x, focal_y, img_path):
        if len(os.path.basename(img_path).split(".")) == 1:
            img_path = img_path + ".png"

        img, h, w = self.get_imgdata_fromfile(img_path)
        pose = torch.tensor(transform_matrix @ converter, dtype=torch.float32)

        self.cameras.append(Camera(pose, focal_x, focal_y, h, w, img_path))

        #world_o = pose[:3, 3]
        #directions = get_ray_directions(h, w, [focal_x, focal_y], center=[w/2.0, h/2.0])  # (h, w, 3)
        #rays = get_rays(directions, pose)  # (h*w, 6)
        #self.img_wh = [w, h]
        #self.poses = torch.cat((self.poses, pose.unsqueeze(0)), dim=0)
        #self.world_o = torch.cat((self.world_o, world_o.unsqueeze(0)), dim=0)
        #self.all_rays.append(rays.unsqueeze(0))

        #self.image_paths += [img_path]
        self.all_rgbs.append(img.cuda())
        self.num += 1

class BlenderDataset():
    def __init__(self, blender_scene_path, json_train_path, ingp_testbed, num_cams, safezone, fov=1.45, split='train', downsample=1.0, N_vis=-1, metric=True):
        self.N_vis = N_vis
        self.generated_data_path = os.path.dirname(json_train_path)
        self.split = split
        self.downsample = downsample
        self.data_samples = DataSamples(downsample)
        #self.define_transforms()

        self.full_json = json_train_path
        if os.path.exists(self.full_json):
            self.read_meta(self.full_json)
        else:
            self.img_wh = (800, 800)
            self.fov = fov
            self.focal_x = 0.5 * self.img_wh[0] / math.tan(0.5 * self.fov)
            self.focal_y = 0.5 * self.img_wh[1] / math.tan(0.5 * self.fov)

        aabb_scale = 16.0
        self.scene_bbox = torch.tensor([[-aabb_scale, -aabb_scale, -aabb_scale], [aabb_scale, aabb_scale, aabb_scale]])
        if blender_scene_path is not None:
            self.geometry = Geometry(os.path.dirname(blender_scene_path), ingp_testbed)
        else:
            self.geometry = Geometry(os.path.dirname(json_train_path), ingp_testbed)

        try:
            self.blender_scene = BlenderScene(blender_scene_path, output_folder=self.generated_data_path, fov=self.fov, aabb_scale=aabb_scale)
        except:
            print("Could not load Blender Scene")


        if num_cams>0:
            self.add_cameras_random(num_cams, metric=metric, safezone=safezone)
            #self.render_initial_set_hemisphere(num_cams=num_cams, inside_out=True)

        #self.define_proj_mat()

        self.white_bg = True
        self.near_far = [0.1, 100.0]

        self.center = torch.mean(self.scene_bbox, axis=0).float().view(1, 1, 3)
        self.radius = (self.scene_bbox[1] - self.center).float().view(1, 1, 3)
        self.export_json()

    def add_cameras_random(self, num_cams, metric=True, safezone=False):
        if metric:

            xyz = sample_nodes_uniformgrid(self, resolution=32).float().cuda()
            if not safezone:
                density = sample_density_from_ingp_nn(self.geometry.ingp_testbed, self,
                                                      xyz)
                occupancy_mask = density < 0.3

                nodes = xyz[occupancy_mask == True]
            else:
                nodes = xyz
            for i in tqdm(range(num_cams)):
                print(f"{self.fov=}")
                candidate_cams = sample_N_reasonbleCameras(self, 1000, self.fov, safezone)
                metrics = torch.tensor(
                    [compute_KSMetric_PerNode(self, self.data_samples.cameras + [cam], nodes)[0].sum() for cam in
                     candidate_cams])
                torch.cuda.empty_cache()
                selected_camera = candidate_cams[metrics.argmax()]
                print(f"{selected_camera.pose_mat=}")
                eye, look_at, _ = get_eye_lookat_from_pose(selected_camera.pose_mat)
                frame_data = self.blender_scene.render_camera(eye.cpu().detach().numpy(), look_at.cpu().detach().numpy(), os.path.join(self.split, f"cam_{self.data_samples.num}.png"))
                self.data_samples.add_image(transform_matrix=np.array(frame_data['transform_matrix']),
                                            focal_x=self.focal_x,
                                            focal_y=self.focal_y,
                                            img_path=os.path.join(self.blender_scene.generated_data_path, frame_data['file_path']))
                #self.data_samples.cameras[-1].wierd_mat = selected_camera.wierd_mat


        else:
            selected_cameras = sample_N_reasonbleCameras(self, min(1000, num_cams*4), self.fov, safezone)[:num_cams]
            torch.cuda.empty_cache()
            for cam in tqdm(selected_cameras):
                eye, look_at, _ = get_eye_lookat_from_pose(cam.pose_mat)
                torch.cuda.empty_cache()
                frame_data = self.blender_scene.render_camera(eye.cpu().detach().numpy(),
                                                              look_at.cpu().detach().numpy(), os.path.join(self.split, f"cam_{self.data_samples.num}.png"))
                torch.cuda.empty_cache()
                self.data_samples.add_image(transform_matrix=np.array(frame_data['transform_matrix']),
                                            focal_x=self.focal_x,
                                            focal_y=self.focal_y,
                                            img_path=os.path.join(self.generated_data_path, frame_data['file_path']))
                torch.cuda.empty_cache()
        #visualize_poses(None, torch.stack([cam.pose_mat for cam in self.data_samples.cameras]))

    def add_new_camera(self):
        eye, look_at, _ = self.camera_sampler.get_next()
        frame_data = self.blender_scene.render_camera(eye.cpu().detach().numpy(),
                                                      look_at.cpu().detach().numpy(), self.split)

        self.data_samples.add_image(transform_matrix=np.array(frame_data['transform_matrix']),
                                    focal_x=self.focal_x,
                                    focal_y=self.focal_y,
                                    img_path=os.path.join(self.generated_data_path, frame_data['file_path']))

    def render_initial_set_hemisphere(self, num_cams=3, inside_out=False):
        print("Rendering initial set of images")
        for i in tqdm(range(0, num_cams)):
            # Sample location
            pos = self.blender_scene.get_random_hemisphere_loc(self.geometry.roi_radius*1.5) + self.geometry.roi_center
            # Sample direction (Always looking either in the center or the opposite)
            d = 1.0 if inside_out else -1.0
            direction = d * (np.array(self.geometry.roi_center) - pos)
            # Render
            rel_path = os.path.join(self.split, "cam_{}.png".format(len(self.data_samples.cameras)))
            frame_data = self.blender_scene.render_camera(pos, direction, rel_path)
            #self.blender_scene.render_depth(pos, direction)

            self.data_samples.add_image(transform_matrix=np.array(frame_data['transform_matrix']),
                                        focal_x=self.focal_x,
                                        focal_y=self.focal_y,
                                        img_path=os.path.join(self.generated_data_path, frame_data['file_path']))


    def get_num_cameras(self):
        return self.data_samples.num

    def get_all_rays(self):
        return self.data_samples.all_rays
    def get_all_rgbs(self):
        return self.data_samples.all_rgbs

    def to(self, device):
        self.data_samples.to(device)

    def export_geometry(self, out_dir):
        print("Exporting Geometry...", end="")
        self.blender_scene.export_geometry(out_dir)
        print("Done")

    def read_meta(self, json_file):
        with open(json_file, 'r') as f:
            self.meta = json.load(f)

        w, h = int(self.meta['w'] / self.downsample), int(self.meta['h'] / self.downsample)
        self.img_wh = [w, h]
        self.fov = self.meta['camera_angle_x']
        self.focal_x = 0.5 * w / np.tan(0.5 * self.meta['camera_angle_x'])  # original focal length
        self.focal_y = 0.5 * h / np.tan(0.5 * self.meta['camera_angle_y'])  # original focal length
        self.cx, self.cy = self.meta['cx'], self.meta['cy']

        #self.intrinsics = torch.tensor([[self.focal_x, 0, self.cx], [0, self.focal_y, self.cy], [0, 0, 1]]).float()


        img_eval_interval = 1 if self.N_vis < 0 else len(self.meta['frames']) // self.N_vis
        idxs = list(range(0, len(self.meta['frames']), img_eval_interval))
        for i in tqdm(idxs, desc=f'Loading data {self.split} ({len(idxs)})'):  # img_list:#
            frame = self.meta['frames'][i]
            self.data_samples.add_image(transform_matrix=np.array(frame['transform_matrix']),
                                        focal_x=self.focal_x,
                                        focal_y=self.focal_y,
                                        img_path=os.path.join(os.path.dirname(json_file), frame['file_path']))


    def export_json(self, aabb_scale=16):

        scene_dictionary = {
            'camera_angle_x': self.fov,
            'camera_angle_y': self.fov,
            'w': self.img_wh[0],
            'h': self.img_wh[1],
            'cx': self.img_wh[0] / 2.0,
            'cy': self.img_wh[1] / 2.0,
            'aabb_scale': aabb_scale,
            'frames': []
        }

        for camera in self.data_samples.cameras:
            frame_data = {'file_path': camera.img_path, 'rotation': 0.0,
                          'transform_matrix': listify_tensor(camera.pose_mat.cpu() @ converter)}
            scene_dictionary["frames"].append(frame_data)
        print(scene_dictionary)
        with open(self.full_json, 'w') as out_file:
            json.dump(scene_dictionary, out_file, indent=4)