# Improving NeRF Quality by Progressive Camera Placement for Unrestricted Navigation in Complex Environments
> Vision, Modeling, and Visualization, 2023  
> [Georgios Kopanas](https://grgkopanas.github.io/), [George Drettakis](https://www-sop.inria.fr/members/George.Drettakis/)\
> [Project page]()&nbsp;/ [Paper](https://arxiv.org/abs/2309.00014)&nbsp;

This source code is forked from [Instant-NGP](https://nvlabs.github.io/instant-ngp). Original licence terms apply for any code from the original repo.

For the purposes of the implementation for this paper we used our algorithm on top of instant-ngp and blender. Intant-NGP is used as our NeRF model and Blender is used to render new views while instant-ngp is training. For more details please refer to the paper.

## Installation
All packages need to be installed in the python distribution provided with Blender. In my Windows setup this lives here:
```
C:\Program Files\Blender Foundation\Blender 3.3\3.3\python\bin
```

To install any standard python package you can run:
```
#Open PowerShell as admin
cd C:\Program Files\Blender Foundation\Blender 3.3\3.3\python\bin
#install packages
./python.exe -m pip install imageio[ffmpeg] -t ..\lib\site-packages\
```

First follow all the steps in the original Instant-NGP repository[here](https://nvlabs.github.io/instant-ngp) and install python packages in the Blender's python.

Then install the custom histogram cuda implementation:
```
cd  scripts/histogram_batched
C:\Program Files\Blender Foundation\Blender 3.3\3.3\python\bin\python setup.py install 
```

## Run

```
&"C:\Program Files\Blender Foundation\Blender 3.3\blender.exe" --background --python ./scripts/run.py -- --blender_scene [path_to_blender_scene.blend] --output_json_scene [output_folder] --start_cams 20 --n_steps 35000
```

Next to the .blend file we expect to have 3 ply files with a single cuboid:

* scene_aabb.ply: describes the bounding box where cameras are allowed to be places
* scene_safezone.ply: describes the bounding box of a safe, unoccupied zone, where the first initial N cameras are going to be placed.
* scene_centerobject.ply: describes the bounding box of a center objects for the "hemisphere" capture in the paper

## Scenes
Unfortunately we cannot provide the scenes we used in the paper because of their licensing terms.
